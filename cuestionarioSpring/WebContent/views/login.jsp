<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EASY SURVEY - Login</title>
</head>
<body>
	<center>
	<h2>LOGIN</h2>
		<form:form method="POST" action="/cuestionarioSpring/ingresando">
		   <table>
			    <tr>
			        <td><form:label path="username">Usuario: </form:label></td>
			        <td><form:input path="username" /></td>
			    </tr>
			    <tr>
			        <td><form:label path="password">Contraseņa: </form:label></td>
			        <td><form:input path="password" /></td>
			    </tr>
			    <tr>
			        <td colspan="2">
			            <input type="submit" value="INGRESAR"/>
			        </td>
			    </tr>
			</table>  
		</form:form>
	</center>	
</body>
</html>