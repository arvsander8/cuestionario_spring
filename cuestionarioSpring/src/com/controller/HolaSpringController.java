package com.controller;
 
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
 
@Controller
public class HolaSpringController {
 
    @RequestMapping("/holaMundo")
    public ModelAndView holaMundoSpring() {
         ModelAndView modelAndView = new ModelAndView();
         String mensaje = "Hola Programacion Web !!!";
         modelAndView.addObject("mensaje", mensaje);
         modelAndView.setViewName("holaSpring");
         return modelAndView;
    }
 
}