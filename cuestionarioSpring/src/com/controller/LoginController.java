package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.ModelMap;

import com.object.Usuario;
import com.dao.UsuarioDAO;

@Controller
public class LoginController {

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	   public ModelAndView student() {
	      return new ModelAndView("login", "command", new Usuario());
	   }
	   
	   @RequestMapping(value = "/ingresando", method = RequestMethod.POST)
	   public String addStudent(@ModelAttribute("SpringWeb")Usuario usuario, 
	   ModelMap model) {
		  //aqu� procesamos los datos 
		  UsuarioDAO usudao = new UsuarioDAO(); 
		  String nick = "nada";
		  try {
				nick = usudao.verifiacarDatos(usuario.getUsername(),usuario.getPassword());
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  if(nick != ""){
	    	  model.addAttribute("username", nick);
		      return "lista_cuestionarios";
	      }else{
	    	  //error
	    	  model.addAttribute("username", usuario.getUsername());
		      model.addAttribute("password", usuario.getPassword());
	    	  return "error_login";
	      }
	      
	   }
	
}
